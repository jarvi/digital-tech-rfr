import GetPosts from "../../helpers/getPosts"

export async function fetchDataReal(options) {
  let data = JSON.parse(localStorage.getItem("localdata"))
    ? JSON.parse(localStorage.getItem("localdata"))
    : localStorage.setItem("localdata", JSON.stringify(await GetPosts()))

  let localData = data.reverse()

  return {
    rows: localData.slice(
      options.pageIndex * options.pageSize,
      (options.pageIndex + 1) * options.pageSize,
    ),
    pageCount: Math.ceil(data.length / options.pageSize),
  }
}
