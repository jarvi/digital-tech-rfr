import { faker } from "@faker-js/faker"

const range = len => {
  const arr = []
  for (let i = 0; i < len; i++) {
    arr.push(i)
  }
  return arr
}

const newPost = () => {
  return {
    createdAt: faker.date.past(),
    image: faker.image.avatar(),
    message: faker.internet.password(),
    likes: [],
    author: "rangel",
    location: "indefinido",
    status: faker.helpers.shuffle(["published", "deleted", "single"])[0],
    id: faker.number.int(),
    description: "",
  }
}

export function makeData(...lens) {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth]
    return range(len).map(d => {
      return {
        ...newPost(),
        subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined,
      }
    })
  }

  return makeDataLevel()
}

const data = makeData(10000)

export async function fetchData(options) {
  // Simulate some network latency
  await new Promise(r => setTimeout(r, 500))

  // //console.log("resultado:");
  // //console.log(Math.ceil(data.length));
  // //console.log(Math.ceil(data.length / options.pageSize));
  return {
    rows: data.slice(
      options.pageIndex * options.pageSize,
      (options.pageIndex + 1) * options.pageSize,
    ),
    pageCount: Math.ceil(data.length / options.pageSize),
  }
}
