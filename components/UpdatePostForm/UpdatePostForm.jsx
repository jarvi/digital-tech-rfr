import { useState, useEffect, useCallback, useMemo, memo } from "react"
import { useLocalStorage } from "../../hooks/useLocalStorage"
import { default as NextImage } from "next/image"
//import { getDataUser } from "../../hooks/useInformationClient";
import { LOCAL_STORAGE_KEYS, APIS, END_POINTS } from "../../util/constants"
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import validateLink from "../../helpers/validateLink"
import useData from "../../hooks/useData"
import * as yup from "yup"
//import UpdateGetPosts from "../../helpers/updatePosts";
import useUpdatePosts from "../../hooks/useUpdatePosts"
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import { updatePost } from "../../helpers/updatePost"

const schema = yup
  .object({
    author: yup
      .string()
      .required("Este campo es requerido")
      .min(3, "debe colocar minimo 3 caracteres")
      .max(20, "Este debe llevar maximo 20 caracteres")
      .matches(/^[a-zA-Z]+$/, "Solo se admiten letras"),
    image: yup.string(),
    message: yup
      .string()
      .required("Este campo es requerido")
      .min(10, "debe colocar minimo 10 caracteres")
      .max(500, "Este debe llevar maximo 500 caracteres"),
  })
  .required()

function UpdatePostForm() {
  const queryClient = useQueryClient()

  ////console.log("UpdatePostForm");
  const { objCollection: dataLoggedUser } = useLocalStorage(
    LOCAL_STORAGE_KEYS.USER_LOGGED,
  )
  const { objCollection: dataLocationUser } = useLocalStorage(
    LOCAL_STORAGE_KEYS.USER_LOCATION,
  )

  const [imgSelected, setImgSelected] = useState(null)

  const {
    tanStackAdminTableRowSelect,
    setModalVisibility,
    tanStackAdminTableState,
  } = useData()

  const handleimgSelected = useCallback(async url => {
    const imgValidated = await validateLink(url)
    //////console.log(imgValidated);
    setImgSelected(imgValidated)
  }, [])

  const {
    setValue,
    getValues,
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
    defaultValues: {
      author: tanStackAdminTableRowSelect.author,
      image: tanStackAdminTableRowSelect.image,
      message: tanStackAdminTableRowSelect.message,
      status: tanStackAdminTableRowSelect.status,
    },
  })

  const statusPost = ["rented", "sold", "drafted", "deleted", "published"]

  const { mutate, error, isLoading } = useMutation(updatePost)

  const onSubmit = async capturedFormData => {
    if (isValid) {
      //capturedFormData.image = imgSelected ? imgSelected : formPostData.image;
      const updatePost = {
        id: tanStackAdminTableRowSelect.id,
        createdAt: new Date().getTime(),
        likes: tanStackAdminTableRowSelect.likes,
        author: tanStackAdminTableRowSelect.author,
        location: tanStackAdminTableRowSelect.location,
        ...capturedFormData,
      }

      mutate(updatePost, {
        onSuccess: resposeData => {
          const currentPageIndex = tanStackAdminTableState.pagination.pageIndex
          const currentPageSize = tanStackAdminTableState.pagination.pageSize
          const currentIdPost = tanStackAdminTableState.rowSelection.id
          const queryResult = queryClient.getQueryData({
            queryKey: [
              "tableData",
              { pageIndex: currentPageIndex, pageSize: currentPageSize },
            ],
          })

          const modifiedRows = queryResult.rows.map(item => {
            if (item.id === currentIdPost) {
              return resposeData
            }
            return item
          })
          queryClient.setQueryData(
            [
              "tableData",
              { pageIndex: currentPageIndex, pageSize: currentPageSize },
            ],
            data => ({
              ...data,
              rows: modifiedRows,
            }),
          )
          setModalVisibility(x => !x)
        },
      })
    }
  }

  return (
    <>
      <span className="modal-title">Actualice su Post</span>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="form-update-post"
      >
        <div className="formGroup">
          <input
            placeholder="autor"
            className="input"
            type="text"
            {...register("author")}
          />
          {errors.author?.message && (
            <p className="message">{errors.author?.message}</p>
          )}
        </div>

        <div className="formGroup">
          <input
            placeholder="url img"
            className="input"
            type="text"
            {...register("image", {
              onBlur: e => handleimgSelected(getValues("image"), e),
            })}
          />
          {errors.image?.message && (
            <p className="message">{errors.image?.message}</p>
          )}
        </div>

        {imgSelected && (
          <div className="showImg">
            <NextImage
              src={imgSelected}
              alt="title"
              width={200}
              height={200}
              className="post-image"
              priority
            />
          </div>
        )}

        <div className="formGroup">
          <select
            className="select-status"
            {...register("status")}
          >
            <option
              className="disabled-select"
              value="drafted"
              disabled
            >
              Seleccione...
            </option>
            {statusPost.map((currentValue, i) => (
              <option
                key={i}
                value={currentValue}
              >
                {currentValue}
              </option>
            ))}
          </select>
        </div>

        <div className="formGroup">
          <textarea
            className="textarea"
            {...register("message")}
            placeholder="message"
          ></textarea>
          {errors.message?.message && (
            <p className="message">{errors.message?.message}</p>
          )}
        </div>

        <div className="actions">
          {/* <span>{response?.statusText}</span> */}
          <button
            disabled={!isValid}
            type="submit"
            className={isValid && !isLoading ? "button" : "disabled"}
          >
            {isLoading ? <div className="loader"></div> : "Actualizar"}
          </button>
        </div>
      </form>
    </>
  )
}

export default memo(UpdatePostForm)

//export default UpdatePostForm;
