export const deletePost = async id_post => {
  try {
    const response = await fetch(
      "https://643c5bc8f0ec48ce9042d8f2.mockapi.io/digital/posts/" + id_post,
      {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
        },
      },
    )

    const responseBody = await response.json()
    const statusText = response.status

    return { responseBody, statusText }
  } catch (err) {
    //console.log(err)
  }
}
