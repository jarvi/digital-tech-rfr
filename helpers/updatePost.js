export async function updatePost(data) {
  ////console.log(data);
  try {
    const response = await fetch(
      `https://643c5bc8f0ec48ce9042d8f2.mockapi.io/digital/posts/${data.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      },
    )

    const responseBody = await response.json()
    //const statusText = response.statusText;
    return responseBody
  } catch (error) {
    ////console.log(error);
  }
}
